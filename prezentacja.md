---
author: Michał Marchewczyk
title: JavaScript
date: October 2023
theme: Berlin
colortheme: beaver
output: 
  beamer_presentation:
    slide_level: 2
    toc: true
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Table of contents

\tableofcontents



# Introduction to JavaScript

## What is JavaScript?
JavaScript is a versatile, high-level programming language used for
web development. It conforms to the ECMAScript standard.

\begin{block}{Remark}
Although Java and JavaScript are similar in name, syntax, and
respective standard libraries, the two languages are distinct and
differ greatly in design.
\end{block}

## Key features
- high-level
- just-in-time compiled
- dynamic typing
- prototype-based object-orientation
- first-class functions
- mutli-paradigm



# JavaScript essentials

## Primitive data types
There are only 7 primitive data types:

- null
- undefined
- boolean
- number
- BigInt
- string
- symbol

## Objects

In computer science, an object is a value in memory which is
possibly referenced by an identifier. In JavaScript, objects are the
only _mutable_ values. **Functions** are, in fact, also objects with the
additional capability of being _callable_.

## Variables
JavaScript allows you to declare variables using **var**, **let**, or **const**.
These keywords define the scope and mutability of a variable,
providing control over how data is stored and accessed within your
programs.

```javascript
var test1 = 'abc';
let test2 = 123;
const test3 = true;
```

## Operators
JavaScript offers a wide range of operators for performing
operations on variables and values. Key operator categories
include:

- **Arithmetic Operators**: These encompass addition (+),
subtraction (-), multiplication (*), division (/), and the
modulus operator (%), used for fundamental mathematical
operations.
- **Comparison Operators**: JavaScript supports operators for
comparing values, such as equality (== and ===), inequality
(!= and !==), greater than (>), and less than (<), essential
for making logical decisions based on data comparisons.

## Operators (cont.)
- **Logical Operators**: Logical operators like AND (&&), OR
(||), and NOT (!) enable you to create complex logical
conditions, crucial for implementing branching logic and
conditional statements.
- **Assignment Operators**: These operators, including simple
assignment (=) and compound assignments (e.g., +=, -=,
*=), are used to assign values to

## Operators examples
```javascript
let a = 1 + 2; // 3
let b = 5 - 2; // 3
let c = 3 * 4; // 12
let d = 6 / 2; // 3
let e = 7 % 2; // 1

let f = 1 == 1; // true
let g = 1 != 1; // false
let h = 4 > 3; // true
let i = 4 < 3; // false

let j = true && false; // false
let k = true || false; // true
```

# JavaScript in action

## Web development
JavaScript is the cornerstone of modern web development, where it
plays a pivotal role in enhancing user experiences. Through
JavaScript, developers can create dynamic and interactive web
pages.

There are many JavaScript frameworks and libraries for web development,
such as React, Angular, Vue, and jQuery.

![React](img/react.png){height=25% width=25%}
![Angular](img/angular.png){height=25% width=25%}
![Vue](img/vue.png){height=25% width=25%}

## Server-side development
Node.js, a JavaScript runtime environment, has ushered in a new
era of server-side development. It enables developers to write
server-side code in JavaScript, creating a unified full-stack
development experience.

![](img/nodejs.png){height=40% width=40%}
